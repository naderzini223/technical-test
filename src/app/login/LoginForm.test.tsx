import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import LoginForm from "./page"; // Update the import path

describe("LoginForm Component", () => {
  it("renders username and password inputs", () => {
    render(<LoginForm />);
    expect(screen.getByLabelText("Username")).toBeInTheDocument();
    expect(screen.getByLabelText("Password")).toBeInTheDocument();
  });

  it("renders Remember Me checkbox", () => {
    render(<LoginForm />);
    expect(screen.getByLabelText("Remember Me")).toBeInTheDocument();
  });

  it("submits form with username and password", () => {
    const handleSubmit = jest.fn();
    render(<LoginForm onSubmit={handleSubmit} />); // Pass onSubmit prop

    const usernameInput = screen.getByLabelText("Username") as HTMLInputElement;
    const passwordInput = screen.getByLabelText("Password") as HTMLInputElement;
    const submitButton = screen.getByRole("button", { name: "Submit" });

    fireEvent.change(usernameInput, { target: { value: "testuser" } });
    fireEvent.change(passwordInput, { target: { value: "testpassword" } });
    fireEvent.click(submitButton);

    expect(handleSubmit).toHaveBeenCalledWith({
      username: "testuser",
      password: "testpassword",
    });
  });
});