// @ts-nocheck
'use client'
import React, { useState } from 'react';
import styles from './LoginForm.module.css';
import {authentification} from '../../tools/api'
import { useRouter } from "next/navigation";



const LoginForm: React.FC = () => {
  const router = useRouter()
  const token = localStorage.getItem("authToken");
  const isAuthenticated = !!token;

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [rememberMe, setRememberMe] = useState(false);
  const [formError, setFormError] = useState(false);

  const [errorMessage, setErrorMessage] = useState("");

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    setErrorMessage('');
  
    if (!username || !password) {
      setErrorMessage("Please fill in all fields.");
      return;
    }
    

    try {
      const response = await authentification({
        username, 
        password,
      });
      console.log(response)
      if (response.status === 200) {
        const  token  = response.data.token;
        localStorage.setItem("authToken", token);
        router.push("/dashboard");
        setErrorMessage("");
        setFormError('')
      }
    } catch (error) {
      console.error("Error:", error);
      setErrorMessage('');
      setFormError(error.response.data.message);
    }
  };

  if (isAuthenticated) {
    router.replace("/dashboard"); // Redirect to login page if not authenticated
  }else{
    return (
      <div className={styles["login-form"]}>
        <h1>Login Form</h1>
        <form onSubmit={handleSubmit}>
        <div className={styles["form-group"]}>
            <label className={styles["form-label"]} htmlFor="username">Username</label>
            <input
              type="text"
              className={styles["form-input"]}
              id="username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div className={styles["form-group"]}>
            <label htmlFor="password">Password</label>
            <input
              type="password"
              id="password"
              className={styles["form-input"]}
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
           {errorMessage && (
            <div className={styles["error-message"]}>{errorMessage}</div>
          )}
          </div>
          <div className={styles["remember-me"]}>
            <label>
              <input
                type="checkbox"
                checked={rememberMe}
                onChange={() => setRememberMe(!rememberMe)}
              />
              Remember Me
            </label>
          </div>
          <div>
          <button className={styles["submit-btn"]} type="submit">Submit</button>
          </div>
        </form>
        {formError && <span className={styles["error-message"]}>{formError}</span>}
        </div>
    );
  }
};

export default LoginForm;