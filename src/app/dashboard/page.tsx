'use client'
import React, { useEffect } from "react";
import { useRouter } from "next/navigation";
import styles from "./dashboard.module.css";

const Dashboard = () => {
  const router = useRouter();
  const token = localStorage.getItem("authToken");
  const isAuthenticated = !!token;
  const handleLogout = () => {
    
    localStorage.removeItem("authToken");
    router.replace("/login");
  };
    if (!isAuthenticated) {
      router.replace("/login"); // Redirect to login page if not authenticated
    }
  else{
    return (
      <div className={styles.container}>
        <header className={styles.header}>
          <h1>Welcome to the Dashboard</h1>
          <button className={styles.logoutButton} onClick={handleLogout}>
            Log Out
          </button>
        </header>
        <main className={styles.mainContent}>
          <p>Your dashboard content goes here.</p>
        </main>
      </div>
    );
  }

 

  
};

export default Dashboard;
