import React from "react";
import { render, screen } from "@testing-library/react";
import Dashboard from "./page";

describe("Dashboard Component", () => {
  it("renders the dashboard title", () => {
    render(<Dashboard />);
    const titleElement = screen.getByText("Dashboard");
    expect(titleElement).toBeInTheDocument();
  });

  it("renders the welcome message", () => {
    render(<Dashboard />);
    const messageElement = screen.getByText("Welcome to the dashboard page!");
    expect(messageElement).toBeInTheDocument();
  });
});