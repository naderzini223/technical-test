import axios from "axios";
import { LoginCredentials } from "./types"; 


const authentification = (credentials: LoginCredentials) =>
  axios.post(`https://dummyjson.com/auth/login`, credentials, {
    headers: { "Content-Type": "application/json" },
  });

  export {
    authentification
  }